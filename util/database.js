const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;

let _db;

const mongoConnect = (callback) => {
    // mongodb://dbUser:dbUserPassword1@ds062797.mlab.com:62797/node-complete
    MongoClient.connect('mongodb+srv://dbUser:dbUserPassword@demo-ltmsk.azure.mongodb.net/test?retryWrites=true&w=majority')
        .then((client) => {
            console.log('Connected!');
            _db = client.db();
            callback();
        })
        .catch((err) => {
            console.log(err);
            throw err;
        });
};

const getDb = () => {
    if (_db) {
        return _db;
    }
    throw 'No DataBase found!';
}

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;