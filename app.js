const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const pageNotFoundController = require('./controllers/page-not-found');

const mongoConnect = require('./util/database').mongoConnect;
const User = require('./models/user');

const app = express();

// Setup views engine
app.set('view engine', 'ejs');
app.set('views', 'views');

// Import routes
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');

app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.use((req, res, next) => {
    User.findById('5d0c6a0e1c9d440000d56217')
        .then((user) => {
            req.user = new User (user.name, user.email, user.cart, user._id);
            next();
        })
        .catch((err) => {
            console.log(err);
        });
})

// Routes
app.use('/admin', adminRoutes);
app.use(shopRoutes);

// 404
app.use(pageNotFoundController);

mongoConnect(() => {
    app.listen(3000);
});